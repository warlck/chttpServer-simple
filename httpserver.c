#include <arpa/inet.h>
#include <dirent.h>
#include <errno.h>
#include <fcntl.h>
#include <netdb.h>
#include <netinet/in.h>
#include <pthread.h>
#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <unistd.h>
#include <sys/ioctl.h>

#include "libhttp.h"
#include "wq.h"





/*
 * Global configuration variables.
 * You need to use these in your implementation of handle_files_request and
 * handle_proxy_request. Their values are set up in main() using the
 * command line arguments (already implemented for you).
 */
wq_t work_queue;
int num_threads;
int server_port;
char *server_files_directory;
char *server_proxy_hostname;
int server_proxy_port;


pthread_mutex_t lock;
pthread_cond_t client_bufflen_cond;
pthread_cond_t server_bufflen_cond;




struct data {
  char **server_buffer;
  char **client_buffer;
  int server_socket_fd;
  int client_socket_fd;
  int *server_bufflen;
  int *client_bufflen;
};





void read_file(char *path, char **buffer, long *lsize, int *status) {
  FILE * pFile;
  size_t result;
  long lSize;


  pFile = fopen(path, "rb");
  if (pFile == NULL) { fputs("File error", stderr); *status = 500; return; }

  fseek(pFile, 0, SEEK_END);
  lSize = ftell(pFile);
  rewind(pFile);

  *buffer = (char *) malloc(sizeof(char)*lSize);
  if (buffer == NULL) { fputs("Memory error", stderr); *status = 500; return; }

  result = fread(*buffer, 1, lSize, pFile);
  if (result != lSize) { fputs("Reading error", stderr); *status = 500; return; }

  *lsize = lSize;
  fclose(pFile);
}






static int index_file_exists(const struct dirent *ep) {
  if (strcmp(ep->d_name, "index.html") == 0)
	return 1;
  return 0;
}



static int one (const struct dirent *unused) {
  return 1;
}



void error(char *msg) {
  fprintf(stderr, "%s: %s\n", msg, strerror(errno));
  exit(1);
}


void replace_host(char **buffer) {
	char *read_buffer = *(buffer);
	char *read_start, *read_end;

	// Hostname we are substituting in header
	// 7 = 1 for colon, 5 for port number (max is ~65k), 1 for null
	char hostname[strlen(server_proxy_hostname)+7];
	char temp_buff[strlen(read_buffer)];
	strcpy(temp_buff, read_buffer);



	size_t read_size;

	read_start = strstr(temp_buff, "Host:");
	if (read_start == NULL) return;
	
	read_start += 6;
	read_end = read_start;
	while (*read_end != '\r') 
		read_end++;
	// read_end +=2;

	sprintf(hostname, "%s:%d",server_proxy_hostname,server_proxy_port);
	int offset = read_start - temp_buff;
	memset(read_buffer+offset, 0, strlen(read_start));
	memcpy(read_buffer+offset, hostname, strlen(hostname));
	memcpy(read_buffer+offset + strlen(hostname), read_end, strlen(read_end));

	// read_size = read_end - read_start;
	// memset(read_start,' ', read_size);
	// 
	// memcpy(read_start, hostname, strlen(hostname));
}


int open_socket(char *host, char *port) {
  struct addrinfo *res;
  struct addrinfo hints;
  memset(&hints, 0, sizeof(hints));

  hints.ai_family = PF_UNSPEC;
  hints.ai_socktype = SOCK_STREAM;
  if (getaddrinfo(host, port, &hints, &res) == -1) {
	error("Can't resolve the address");
  }

  int d_sock = socket(res->ai_family, res->ai_socktype, res->ai_protocol);
  if (d_sock == -1)
	error("Can't open socket");

  int c = connect(d_sock,  res->ai_addr, res->ai_addrlen);
  freeaddrinfo(res);

  if (c == -1)
	error("Can't connect to socket");
  return d_sock;
}



/*
 * Reads from socket into buffer. Assumption is that input buffer 
 * has initial allocated space more then 1024.
 * If input buffer's size is not enough, reallocates the buffer to 
 * double of original size;
 */
int read_in(int fd, char **buffer, int *len, int init_bufflen) {
  char *s = *buffer;
  char *string_end  = NULL;
  int bufflen = init_bufflen;
  int count = 0;
  int TEMP_BUFF_LEN = 1024;
  char temp_buff[TEMP_BUFF_LEN];
  int available = 0;
  memset(temp_buff, 0, TEMP_BUFF_LEN);
  int bytes_read = 0;

  // int bytes_read = recv(fd, temp_buff, TEMP_BUFF_LEN, 0);
  // count += bytes_read;
  // memcpy(s, temp_buff, bytes_read);

  while (1) {



	bytes_read = recv(fd, temp_buff, TEMP_BUFF_LEN, 0);
	count += bytes_read;

	if (bytes_read == 0)
		break;

	if (count > bufflen) {
	  s = (char *)realloc(*buffer, count + 1);
	  bufflen = count+1;
	  *buffer = s;
	}
	memcpy(s + count - bytes_read, temp_buff, bytes_read);

	// string_end = strstr(s+count-10,"\r\n\r\n");
	// if (string_end != NULL )
	//   break;


	memset(temp_buff, 0, TEMP_BUFF_LEN);

	ioctl(fd, FIONREAD, &available);
	if (available <= 0)
		break;
  }


  *len = count;

  if (count < 0)
	return count;
  else if (count == 0)
	(*buffer)[0] = '\0';
  else 
	(*buffer)[count] = '\0';
  return count;

}


/*
 * Reads an HTTP request from stream (fd), and writes an HTTP response
 * containing:
 *
 *   1) If user requested an existing file, respond with the file
 *   2) If user requested a directory and index.html exists in the directory,
 *      send the index.html file.
 *   3) If user requested a directory and index.html doesn't exist, send a list
 *      of files in the directory with links to each.
 *   4) Send a 404 Not Found response.
 */
void handle_files_request(int fd) {

  /*
   * TODO: Your solution for Task 1 goes here! Feel free to delete/modify *
   * any existing code.
   */

  struct http_request *request = http_request_parse(fd);
  int status = 200;
  char mime[30];
  char path[1000];
  memset(path, 0, sizeof(path));
  strcpy(path, server_files_directory);
  strcat(path, request->path);
  struct stat statbuf;


  
  if (stat(path, &statbuf) == -1) {
	  perror("static");
	  status = 404;
	  http_start_response(fd, status);
	  http_send_header(fd, "Content-Type", "text/plain");
	  http_end_headers(fd);
	  http_send_string(fd,"404 Not Found");
	  return;
  }



  int reg = S_ISREG(statbuf.st_mode);
  int dir = S_ISDIR(statbuf.st_mode);



  long lSize;
  char * buffer = NULL;
  

  if (reg) {
	read_file(path, &buffer, &lSize, &status);
	sprintf(mime, http_get_mime_type(path));
  }

  if (dir) {
	struct dirent **eps;
	int n;

	n = scandir(path, &eps, index_file_exists, alphasort);
	if (n > 0) {
	  strcat(path, "index.html");
	  read_file(path, &buffer, &lSize, &status);
	} else {
	  n = scandir(path, &eps, one, alphasort);
	  if (n >= 0) {
		  buffer = malloc(1000);
		  if (!buffer) {
			error("Malloc Failed");
		  }

		  memset(buffer, 0, 1000);
		  int length = 0;
		  length += sprintf(buffer,"<p><a href='../'>Parent Directory</a></p>");
		  for (int cnt = 2; cnt < n; ++cnt) {
			length += sprintf(buffer + length, "<p><a href='./%s'>%s</a></p>", 
										  eps[cnt]->d_name, eps[cnt]->d_name);
		  }
		  lSize = length;  
	  }
	  else  {
		perror("Couldn't open the directory");
		status = 404;
	  }
	}
	 sprintf(mime, "text/html");
  }
  




  if (status == 200) {
	http_start_response(fd, 200);
	http_send_header(fd, "Content-Type", mime);
	http_end_headers(fd);
	if (lSize > 0) 
	  http_send_data(fd, buffer, lSize);
	else   
	  http_send_string(fd,
		  "<center>"
		  "<h1>Welcome to httpserver!</h1>"
		  "<hr>"
		  "<p>Nothing's here yet.</p>"
		  "</center>");
  } else {
	http_start_response(fd, status);
	http_end_headers(fd);

  }

  
  // fclose(pFile);
  if (buffer != NULL) {
	free(buffer);
  }

}


void process_client(struct data *thread_data) {
  int count = 0;
  int result = 0;

  while (1) {
	  pthread_mutex_lock(&lock);

	while (result > 0 && *(thread_data->client_bufflen) > 0) {
		if (*(thread_data->server_bufflen) == -1) {
		  pthread_mutex_unlock(&lock);
		  pthread_exit(NULL);
		}

		pthread_cond_wait(&server_bufflen_cond,&lock);
	}

	count = read_in(thread_data->client_socket_fd, thread_data->client_buffer, thread_data->client_bufflen, LIBHTTP_REQUEST_MAX_SIZE);

	// 0 bytes have been received from client socket, meaning it has closed the connection
	// need to signal other thread, unlock mutex and exit current thread.
	if (count  <= 0) {
		*(thread_data->client_bufflen) = -1;
		pthread_cond_signal(&client_bufflen_cond);
		pthread_mutex_unlock(&lock);
		pthread_exit(NULL);
	}
	replace_host(thread_data->client_buffer);
	result = http_send_data(thread_data->server_socket_fd,  *(thread_data->client_buffer), *(thread_data->client_bufflen)); pthread_cond_signal(&client_bufflen_cond);
	pthread_mutex_unlock(&lock);
  }
}

void process_server(struct data *thread_data) {
  int count = 0;

  while (1) {

	pthread_mutex_lock(&lock);

	while(*(thread_data->client_bufflen) <= 0) {
	   if (*(thread_data->client_bufflen) == -1) {
		  pthread_mutex_unlock(&lock);
		  pthread_exit(NULL);
	   }

	   pthread_cond_wait(&client_bufflen_cond,&lock);
	}
	
	count = read_in(thread_data->server_socket_fd, thread_data->server_buffer, thread_data->server_bufflen, LIBHTTP_REQUEST_MAX_SIZE);
	if (count  <= 0) {
		*(thread_data->server_bufflen) = -1;
		pthread_cond_signal(&server_bufflen_cond);
		pthread_mutex_unlock(&lock);
		pthread_exit(NULL);
	}


	http_send_data(thread_data->client_socket_fd, *(thread_data->server_buffer), *(thread_data->server_bufflen));
	*(thread_data->client_bufflen) = 0;
	
	reset_buffers(thread_data);
	pthread_cond_signal(&server_bufflen_cond);
	pthread_mutex_unlock(&lock);
  }
}


void reset_buffers(struct data *thread_data) {
  free(*(thread_data->server_buffer));
  *(thread_data->server_bufflen) = 0;
  char *server_buffer = malloc(LIBHTTP_REQUEST_MAX_SIZE);
  if (!server_buffer) {
	error("Memory error");
  }
  memset(server_buffer, 0, LIBHTTP_REQUEST_MAX_SIZE);
  *(thread_data->server_buffer) = server_buffer;


  free(*(thread_data->client_buffer));
  *(thread_data->client_bufflen) = 0;
  char *client_buffer = malloc(LIBHTTP_REQUEST_MAX_SIZE);
  if (!client_buffer) {
	error("Memory error");
  }
  memset(client_buffer, 0, LIBHTTP_REQUEST_MAX_SIZE);
  *(thread_data->client_buffer) = client_buffer;


}





/*
 * Opens a connection to the proxy target (hostname=server_proxy_hostname and
 * port=server_proxy_port) and relays traffic to/from the stream fd and the
 * proxy target. HTTP requests from the client (fd) should be sent to the
 * proxy target, and HTTP responses from the proxy target should be sent to
 * the client (fd).
 *
 *   +--------+     +------------+     +--------------+
 *   | client | <-> | httpserver | <-> | proxy target |
 *   +--------+     +------------+     +--------------+
 */
void handle_proxy_request(int fd) {
  /*
   * TODO: Your solution for Task 3 goes here! Feel free to delete/modify *
   * any existing code.
   */
  char server_proxy_port_string[6];
  sprintf(server_proxy_port_string,"%d",server_proxy_port);
  char *client_buffer = malloc(LIBHTTP_REQUEST_MAX_SIZE);
  if (!client_buffer) {
	error("Memory error");
  }
  memset(client_buffer, 0, LIBHTTP_REQUEST_MAX_SIZE);

  int server_socket_fd = open_socket(server_proxy_hostname, server_proxy_port_string);
  char *server_buffer = malloc(LIBHTTP_REQUEST_MAX_SIZE);
  if (!server_buffer) {
	error("Memory error");
  }
  memset(server_buffer, 0, LIBHTTP_REQUEST_MAX_SIZE);

  int client_bufflen = 0;
  int server_bufflen = 0;

  struct data thread_arg;
  thread_arg.server_buffer = &server_buffer;
  thread_arg.client_buffer = &client_buffer;
  thread_arg.server_socket_fd = server_socket_fd;
  thread_arg.client_socket_fd = fd;
  thread_arg.server_bufflen = &server_bufflen;
  thread_arg.client_bufflen = &client_bufflen;


  pthread_t client_thread;
  pthread_t server_thread;
  pthread_create(&client_thread, NULL, (void *) &process_client, &thread_arg);
  pthread_create(&server_thread, NULL, (void *) &process_server, &thread_arg);
  pthread_join(client_thread, NULL);
  pthread_join(server_thread, NULL);

  // read_in(fd, &client_buffer, &client_bufflen, LIBHTTP_REQUEST_MAX_SIZE);

  // char req[LIBHTTP_REQUEST_MAX_SIZE];
  // sprintf(req, "GET / HTTP/1.1\r\nHost: %s:%d\r\n\r\n", server_proxy_hostname, server_proxy_port);
  // http_send_string(server_socket_fd, client_buffer);

  // char buffer[4096];
  // int result = send(server_socket_fd, client_buffer, strlen(client_buffer), 0);
  // int n = read(server_socket_fd, buffer, 4095);

  
  // read_in(server_socket_fd, &server_buffer, &server_bufflen, LIBHTTP_REQUEST_MAX_SIZE);

  // http_send_data(fd, server_buffer, server_bufflen);
  free(client_buffer);
  free(server_buffer);
  close(server_socket_fd);
}


/*
 * Opens a TCP stream socket on all interfaces with port number PORTNO. Saves
 * the fd number of the server socket in *socket_number. For each accepted
 * connection, calls request_handler with the accepted fd number.
 */
void serve_forever(int *socket_number, void (*request_handler)(int)) {

  struct sockaddr_in server_address, client_address;
  size_t client_address_length = sizeof(client_address);
  int client_socket_number;

  *socket_number = socket(PF_INET, SOCK_STREAM, 0);
  if (*socket_number == -1) {
	perror("Failed to create a new socket");
	exit(errno);
  }

  int socket_option = 1;
  if (setsockopt(*socket_number, SOL_SOCKET, SO_REUSEADDR, &socket_option,
		sizeof(socket_option)) == -1) {
	perror("Failed to set socket options");
	exit(errno);
  }

  memset(&server_address, 0, sizeof(server_address));
  server_address.sin_family = AF_INET;
  server_address.sin_addr.s_addr = INADDR_ANY;
  server_address.sin_port = htons(server_port);

  if (bind(*socket_number, (struct sockaddr *) &server_address,
		sizeof(server_address)) == -1) {
	perror("Failed to bind on socket");
	exit(errno);
  }

  if (listen(*socket_number, 1024) == -1) {
	perror("Failed to listen on socket");
	exit(errno);
  }

  printf("Listening on port %d...\n", server_port);

  while (1) {
	client_socket_number = accept(*socket_number,
		(struct sockaddr *) &client_address,
		(socklen_t *) &client_address_length);
	if (client_socket_number < 0) {
	  perror("Error accepting socket");
	  continue;
	}

	printf("Accepted connection from %s on port %d\n",
		inet_ntoa(client_address.sin_addr),
		client_address.sin_port);

	request_handler(client_socket_number);
	close(client_socket_number);
  }

  shutdown(*socket_number, SHUT_RDWR);
  close(*socket_number);
}

int server_fd;
void signal_callback_handler(int signum) {
  printf("Caught signal %d: %s\n", signum, strsignal(signum));
  printf("Closing socket %d\n", server_fd);
  if (close(server_fd) < 0) perror("Failed to close server_fd (ignoring)\n");
  exit(0);
}

char *USAGE =
  "Usage: ./httpserver --files www_directory/ --port 8000 [--num-threads 5]\n"
  "       ./httpserver --proxy inst.eecs.berkeley.edu:80 --port 8000 [--num-threads 5]\n";

void exit_with_usage() {
  fprintf(stderr, "%s", USAGE);
  exit(EXIT_SUCCESS);
}

int main(int argc, char **argv) {
  signal(SIGINT, signal_callback_handler);

  /* Default settings */
  server_port = 8000;
  void (*request_handler)(int) = NULL;

  int i;
  for (i = 1; i < argc; i++) {
	if (strcmp("--files", argv[i]) == 0) {
	  request_handler = handle_files_request;
	  free(server_files_directory);
	  server_files_directory = argv[++i];
	  if (!server_files_directory) {
		fprintf(stderr, "Expected argument after --files\n");
		exit_with_usage();
	  }
	} else if (strcmp("--proxy", argv[i]) == 0) {
	  request_handler = handle_proxy_request;

	  char *proxy_target = argv[++i];
	  if (!proxy_target) {
		fprintf(stderr, "Expected argument after --proxy\n");
		exit_with_usage();
	  }

	  char *colon_pointer = strchr(proxy_target, ':');
	  if (colon_pointer != NULL) {
		*colon_pointer = '\0';
		server_proxy_hostname = proxy_target;
		server_proxy_port = atoi(colon_pointer + 1);
	  } else {
		server_proxy_hostname = proxy_target;
		server_proxy_port = 80;
	  }
	} else if (strcmp("--port", argv[i]) == 0) {
	  char *server_port_string = argv[++i];
	  if (!server_port_string) {
		fprintf(stderr, "Expected argument after --port\n");
		exit_with_usage();
	  }
	  server_port = atoi(server_port_string);
	} else if (strcmp("--num-threads", argv[i]) == 0) {
	  char *num_threads_str = argv[++i];
	  if (!num_threads_str || (num_threads = atoi(num_threads_str)) < 1) {
		fprintf(stderr, "Expected positive integer after --num-threads\n");
		exit_with_usage();
	  }
	} else if (strcmp("--help", argv[i]) == 0) {
	  exit_with_usage();
	} else {
	  fprintf(stderr, "Unrecognized option: %s\n", argv[i]);
	  exit_with_usage();
	}
  }

  if (server_files_directory == NULL && server_proxy_hostname == NULL) {
	fprintf(stderr, "Please specify either \"--files [DIRECTORY]\" or \n"
					"                      \"--proxy [HOSTNAME:PORT]\"\n");
	exit_with_usage();
  }

  serve_forever(&server_fd, request_handler);

  return EXIT_SUCCESS;
}
